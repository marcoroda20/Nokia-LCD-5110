void AllAsInput(void);
void PinAsOutputLowPortA(char pin);
void PinAsOutputHighPortA(char pin);
void PinAsOutputLowPortB(char pin);
void PinAsOutputHighPortB(char pin);
void PinAsOutputLowPortC(char pin);
void PinAsOutputHighPortC(char pin);
void PinAsOutputLowPortG(char pin);
void PinAsOutputHighPortG(char pin);
void PinAsOutputLowPortE(char pin);
void PinAsOutputHighPortE(char pin);
void PinAsOutputLowPortF(char pin);
void PinAsOutputHighPortF(char pin);

void PullUp1High(void);
void PullUp1Low(void);
void PullUp2High(void);
void PullUp2Low(void);
void PullUpUEXTLow(void);
void PullUpUEXTHigh(void) ;
int TestExt1(void);
int TestExt2(void);
int TestUEXT(void);

