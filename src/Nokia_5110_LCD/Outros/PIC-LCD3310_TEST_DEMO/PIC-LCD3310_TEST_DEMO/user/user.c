/*********************************************************************
 *
 *                Microchip USB C18 Firmware Version 1.2
 *
 *********************************************************************
 * Author               Date        Comment
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 * Rawin Rojvanit       11/19/04    Original.
 * Fritz Schlunder		08/20/07	A few updates for PIC18F87J50 FS
 *									USB Plug-In Module Support.
 ********************************************************************/

/** I N C L U D E S **********************************************************/
#include <p18cxxx.h>
#include <usart.h>
#include "user\user.h"
#include "system\typedefs.h"

#include "system\usb\usb.h"

#include "io_cfg.h"             // I/O pin mapping
#include "user\user.h"
//#include "user\temperature.h"
#include "lcd.h"

/** V A R I A B L E S ********************************************************/
 unsigned int res_sense,bat_sense;
unsigned char TestUSB_e[]={"USB error\0"};
unsigned char TestUSB_s[]={"USB success\0"};
unsigned char BAT_On[]={"Battery On\0"};
unsigned char BAT_Off[]={"Battery Off\0"};
unsigned char put_BAT[]={"Put Battery \0"};
unsigned char and_press_BUT[]={"and press BUT\0"};

unsigned char Key;

#pragma udata
byte old_sw2,old_sw3;
byte counter;
byte trf_state;
byte temp_mode;

DATA_PACKET dataPacket;

byte pTemp;                     // Pointer to current logging position, will
                                // loop to zero once the max index is reached
byte valid_temp;                // Keeps count of the valid data points
word temp_data[30];             // 30 points of data

// Timer0 - 1 second interval setup.
// Fosc/4 = 12MHz
// Use /256 prescalar, this brings counter freq down to 46,875 Hz
// Timer0 should = 65536 - 46875 = 18661 or 0x48E5
#define TIMER0L_VAL         0xE5
#define TIMER0H_VAL         0x48

/** P R I V A T E  P R O T O T Y P E S ***************************************/

void BlinkUSBStatus(void);
BOOL Switch2IsPressed(void);
BOOL Switch3IsPressed(void);
void ResetTempLog(void);
void ReadPOT(void);
void ServiceRequests(void);

// For board testing purpose only
void PICDEMFSUSBDemoBoardTest(void);

/** D E C L A R A T I O N S **************************************************/
#pragma code
void UserInit(void)
{
  //  mInitAllLEDs();
//    mInitAllSwitches();
//    old_sw2 = sw2;
//	old_sw3 = sw3;				
    
//    InitTempSensor();
//    mInitPOT();
  
 //   ResetTempLog();
//    temp_mode = TEMP_REAL_TIME;
    
    /* Init Timer0 for data logging interval (every 1 second) */
 //   T0CON = 0b10010111;
    //T0CONbits.T08BIT = 0;       // 16-bit mode
    //T0CONbits.T0CS = 0;         // Select Fosc/4
    //T0CONbits.PSA = 0;          // Assign prescalar (default is /256)
    /* Timer0 is already enabled by default */
}//end UserInit


/******************************************************************************
 * Function:        void ProcessIO(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        This function is a place holder for other user routines.
 *                  It is a mixture of both USB and non-USB tasks.
 *
 * Note:            None
 *****************************************************************************/
void ProcessIO(void)
{   
    BlinkUSBStatus();
    // User Application USB tasks
	
	//here you can add your code//////////////////////
//	TRISE = TRISE & 0b11111011;
//	PORTE = PORTE & 0b11111011;
    if((usb_device_state < CONFIGURED_STATE)||(UCONbits.SUSPND==1)) return;

//	#if defined(PIC18F87J50_FS_USB_PIM)
//	PollTempOnHPCExplorer();			
//	#endif

    ServiceRequests();
		
}//end ProcessIO

void ServiceRequests(void)
{
    byte index;
    
    if(USBGenRead((byte*)&dataPacket,sizeof(dataPacket)))
    {
        counter = 0;
        switch(dataPacket.CMD)
        {
            case READ_VERSION:
                //dataPacket._byte[1] is len
                dataPacket._byte[2] = MINOR_VERSION;
                dataPacket._byte[3] = MAJOR_VERSION;
                counter=0x04;
                break;

            case ID_BOARD:
                counter = 0x01;
                if(dataPacket.ID == 0)
                {
			//	TRISE = TRISE & 0b11110111;
			//	PORTE = PORTE & 0b11110111;
    		//  mLED_3_Off();mLED_4_Off();
                }
                else if(dataPacket.ID == 1)
                {
  //                  mLED_3_Off();mLED_4_On();
                }
                else if(dataPacket.ID == 2)
                {
//                    mLED_3_On();mLED_4_Off();
                }
                else if(dataPacket.ID == 3)
                {
      //              mLED_3_On();mLED_4_On();
                }
                else
                    counter = 0x00;
                break;

            case UPDATE_LED:
                // LED1 & LED2 are used as USB event indicators.
                if(dataPacket.led_num == 3)
                {
//                    mLED_3 = dataPacket.led_status;
                    counter = 0x01;
                }//end if
                else if(dataPacket.led_num == 4)
                {
  //                  mLED_4 = dataPacket.led_status;
                    counter = 0x01;
                }//end if else
                break;
                             
            case RESET:
                Reset();
                break;
                
            default:
                break;
        }//end switch()
        if(counter != 0)
        {
            if(!mUSBGenTxIsBusy())
                USBGenWrite((byte*)&dataPacket,counter);
        }//end if
    }//end if

}//end ServiceRequests

/******************************************************************************
 * Function:        void BlinkUSBStatus(void)
 *
 * PreCondition:    None
 *
 * Input:           None
 *
 * Output:          None
 *
 * Side Effects:    None
 *
 * Overview:        BlinkUSBStatus turns on and off LEDs corresponding to
 *                  the USB device state.
 *
 * Note:            mLED macros can be found in io_cfg.h
 *                  usb_device_state is declared in usbmmap.c and is modified
 *                  in usbdrv.c, usbctrltrf.c, and usb9.c
 *****************************************************************************/
void BlinkUSBStatus(void)
{
    //static word led_count=0;
    
    //if(led_count == 0)led_count = 10000U;
    //led_count--;

    

    if(UCONbits.SUSPND == 1)
    {
    }
    else
    {
        if(usb_device_state == DETACHED_STATE)
        {
          //  mLED_Both_Off();
	//	TRISE = TRISE & 0b11111011;
	//	PORTE = PORTE & 0b11111011;

		//	#if defined(PIC18F4550_PICDEM_FS_USB)            
          //  PICDEMFSUSBDemoBoardTest();
		//	#endif
        }
        else if(usb_device_state == ATTACHED_STATE)
        {
	//	TRISE = TRISE & 0b11111011;
	//	PORTE = PORTE & 0b11111011;
       //     mLED_Both_On();
        }
        else if(usb_device_state == POWERED_STATE)
        {
	/*	TRISE = TRISE & 0b11110111;
		PORTE = PORTE & 0b11110111;*/
         //   mLED_Only_1_On();
        }
        else if(usb_device_state == DEFAULT_STATE)
        {
	//			TRISE = TRISE & 0b11111011;
	//			PORTE = PORTE & 0b11111011;
           // mLED_Only_2_On();
        }
        else if(usb_device_state == ADDRESS_STATE)
        {
		//		TRISE = TRISE & 0b11111011;
		//		PORTE = PORTE & 0b11111011;
			
        }
        else if(usb_device_state == CONFIGURED_STATE)
        {
			/*	TRISE = TRISE & 0b11111011;
				PORTE = PORTE & 0b11111011;*/
				LCDStr(0,TestUSB_s,0);
				if (!check_bat_sense())	LCDStr(1,BAT_Off,0);//LCDStr(1,BAT_On,0);else	LCDStr(1,BAT_Off,0);
				LCDStr(2,put_BAT,0);
				LCDStr(3,and_press_BUT,0);
				do
				{			
					Key = GetJoystickPosition();
				}
				while(Key == 0);
				if (check_bat_sense())	LCDStr(4,BAT_On,0);
				else LCDStr(4,BAT_Off,0);
//				LCDStr(3,BAT_On,0);
           /* if(led_count==0)
            {
                mLED_1_Toggle();
                mLED_2 = !mLED_1;       // Alternate blink                
            }//end if*/

        }//end if(...)
    }//end if(UCONbits.SUSPND...)

}//end BlinkUSBStatus
////////////////////////////////////////////////////////////////////////////////
int check_bat_sense(void)
{
			ANCON0bits.PCFG4=0; 	//AN4 -analog input
			ADCON1=0b10111110;			//right-justified; AD=20AD;A/D clock=fOSC/64 	
			//ADCON1=0x10;
			ADCON0bits.ADON=1; 	//Enable ADC module, AVdref, AVss
			//set BAT_SENSE measure
			ADCON0bits.CHS2=1;
			ADCON0bits.GO=1;				//BSF ADCON0,GO ;Start conversion
			while(ADCON0bits.GO);			//	BSC ADCON0,GO ;Is conversion done? ;	GOTO $-1 ;No, test again
			res_sense=ADRESL;
			bat_sense=ADRESH;
			bat_sense=(bat_sense<<8)|res_sense;
			//res_sense=bat_sense;
			ADCON0=0x00;		//stop ADC
			if(bat_sense>50) return 1;
				else return 0;
}
void PICDEMFSUSBDemoBoardTest(void)
{
	#if defined(PIC18F4550_PICDEM_FS_USB)  
        
	//	UserInit();                     //Re-initialize default user fw
        //Test phase 1 done
    }//end if
	#endif
}//end PICDEMFSUSBDemoBoardTest()

/** EOF user.c ***************************************************************/
