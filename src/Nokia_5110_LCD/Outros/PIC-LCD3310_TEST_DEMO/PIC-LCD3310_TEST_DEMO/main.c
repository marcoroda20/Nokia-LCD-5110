#define MENU_NUMB   6

#include	<p18f67j50.h>
#include <stdio.h>

#include "user\user.h"
#include <string.h>
///#include <stdlib.h>

//#include 	"board.h"
#include 	"lcd.h"
#include 	"mmc.h"
#include    "test.h"

#include "system\typedefs.h"                        // Required
#include "system\usb\usb.h"                         // Required
#include "io_cfg.h"                                 // Required

#include "system\usb\usb_compile_time_validation.h" // Optional
#include "user\user.h"                              // Modifiable



#pragma config XINST    = OFF		// Extended instruction set
        #pragma config STVREN   = OFF		// Stack overflow reset
        #pragma config PLLDIV   = 5			// (20 MHz crystal used on this board)
        #pragma config WDTEN    = OFF		// Watch Dog Timer (WDT)
        #pragma config CP0      = OFF		// Code protect
        #pragma config CPUDIV   = OSC1		// OSC1 = divide by 1 mode
        #pragma config IESO     = OFF		// Internal External (clock) Switchover
        #pragma config FCMEN    = OFF		// Fail Safe Clock Monitor
        #pragma config FOSC     = HSPLL		// Firmware must also set OSCTUNE<PLLEN> to start PLL!
        #pragma config WDTPS    = 32768
//      #pragma config WAIT     = OFF		// Commented choices are
//      #pragma config BW       = 16		// only available on the
//      #pragma config MODE     = MM		// 80 pin devices in the 
//      #pragma config EASHFT   = OFF		// family.
        #pragma config MSSPMSK  = MSK5
//      #pragma config PMPMX    = DEFAULT
//      #pragma config ECCPMX   = DEFAULT
        #pragma config CCP2MX   = DEFAULT





unsigned char Menu[MENU_NUMB][15] = {
 "Test EXT 1&2\0",
 "Test UEXT\0",
 "Test MMC      \0",
 "Test USB      \0",
 "Test MMA      \0",
 "Test Joystick \0"
};

unsigned char Inversion [] ={0,0,0,0,0,0};
unsigned char TestMMC_e[]={"MMC error\0"};
unsigned char Exit[]={"DOWN for EXIT\0"};
unsigned char TestMMC_s[]={"MMC success\0"};
unsigned char CP[]={" MMC: No Card \0"};
unsigned char WP[]={"MMC Write Protect\0"};
unsigned char USB_C[]={"USB Connected\0"};
unsigned char USB_NC[]={"USB Disconnect\0"};
unsigned char MMA_D[]={"MMA Disabled\0"};
//unsigned char Up[]={"Joystick: Up\0"};
//unsigned char Down[]={"Joystick: Down\0"};
//unsigned char Left[]={"Joystick: Left\0"};
//unsigned char Right[]={"Joystick:Right\0"};

unsigned char buf[10];
extern unsigned char VCC_test_state[15];
extern unsigned char GND_test_state[15];
extern unsigned char ext_test_state[20];
extern unsigned char ext_test_state2[20];
extern unsigned char uext_test_state[20];
/////////////////////////////VARIABLES////////////////////////////////
int i,j;

unsigned int resl, resx,resy,resz;
unsigned int x,y,z;


unsigned int sym;

//bank1 extern char mmc_buffer[];
extern unsigned char mydata;
extern char mmc_buffer[128];
 //rom 
 char mmc_buffer_test_1[128];
 char state_mmc  = 1;

unsigned char  offset=0, menu_flag=0;
extern unsigned char status;
signed char new_pos=0, cur_pos=-1;
volatile unsigned long dly;
unsigned char JoyPos;
unsigned char MenuPos;
unsigned char test_state=0;
extern unsigned char vcc_state;
extern unsigned char gnd_state;

void Timer1ISRHigh(void);
//int check_bat_sense(void);

////////////////////////////////////////////////////////////////////////////////////////////
#pragma code high_vector=0x08
void InterruptTimerHigh (void) 
{  _asm    goto Timer1ISRHigh  _endasm
}
#pragma code
////////////////////////////////////////////////////////////////////////////////////////////

#pragma interrupt Timer1ISRHigh
void 
Timer1ISRHigh(void)
{
	//?disable global interrupts prevent second enter to interrupt?
	if(PIR1bits.TMR1IF==1)
		{
		PIR1bits.TMR1IF=0;
		TMR1H=0x80;		//preload for 1 sec 
	//	ToggleLED2();
		PORTEbits.RE3= !PORTEbits.RE3;
		PORTEbits.RE2= !PORTEbits.RE3;
		}
}

////////////////////////FUNCTIONS////////////////////////////////////////////////////////////

		int joystick_right(void)
		{
		//RA4
			if (!(PORTA & 0b00010000))
				return 1;
			else return 0;
		}
////////////////////////////////////////////////////////////////////////////////////////////		
		int joystick_left(void)
		{
			//RB1
			if (!(PORTB & 0b00000010))
				return 1;
			else return 0;
		}
////////////////////////////////////////////////////////////////////////////////////////////		
		int joystick_select(void)
		{
		//RB0
		if (!(PORTB & 0b00000001))
				return 1;
			else return 0;
		}
////////////////////////////////////////////////////////////////////////////////////////////		
		int joystick_up(void)
		{
		//RB4
		if (!(PORTB & 0b00010000))
				return 1;
			else return 0;
		}
////////////////////////////////////////////////////////////////////////////////////////////		
		int joystick_down(void)
		{
		//RB5
		if (!(PORTB & 0b00100000))
				return 1;
			else return 0;
		}
////////////////////////////////////////////////////////////////////////////////////////////		
		void InitLED1(void)
		{
		//RE2
		TRISE = TRISE & 0b11111011;
		PORTE = PORTE & 0b11111011;
		}
////////////////////////////////////////////////////////////////////////////////////////////		
		void InitLED2(void)
		{
		//RE3
		TRISE = TRISE & 0b11110111;
		PORTE = PORTE & 0b11110111;
		}
////////////////////////////////////////////////////////////////////////////////////////////		
		void ToggleLED2(void)
		{
		//RE3
		TRISEbits.TRISE3 ^= 1;
		PORTEbits.RE3 	 ^= 1;
		}

//3/////////////////////////////////////////////////////////////////////////////////////////

	int test_mmc(void)
{
	//card_state |= 1;
			TRISEbits.TRISE0=1;
			TRISEbits.TRISE1=1;
	if((PORTEbits.RE1==0))
	{		
			if((PORTEbits.RE0==0))
			{      
				    if((initMMC() == MMC_SUCCESS));	// card found
					{	
					memset(&mmc_buffer,0x00,128);
					
					mmcReadRegister (10, 16);
		    		mmc_buffer[7]=0;
		
		    		// Fill first Block (0) with 'A'
		   			memset(&mmc_buffer,'0',128);    //set breakpoint and trace mmc_buffer contents
					mmcWriteBlock(0);
		    		// Fill second Block (1)-AbsAddr 512 with 'B'
		    		memset(&mmc_buffer,'1',128);
					mmcWriteBlock(512);
		
		   			 // Read first Block back to buffer
		   			memset(&mmc_buffer,0x00,128);
		    		mmcReadBlock(0,512);
					memset(&mmc_buffer_test_1,'0',128);
		    		if(strncmp(&mmc_buffer[0], &mmc_buffer_test_1[0], 128))
				 		state_mmc=1;
						else state_mmc  = 0;
		   
		    		memset(&mmc_buffer,0x00,128); //set breakpoint and trace mmc_buffer contents
		    		mmcReadBlock(512,512);
					memset(&mmc_buffer_test_1,'1',128);
		    		if(strncmp(&mmc_buffer[0], &mmc_buffer_test_1[0], 128)) 
						state_mmc=1;
						else state_mmc  = 0;

		    		memset(&mmc_buffer,0x00,128); //set breakpoint and trace mmc_buffer contents 	
					}	
			}
			else  { state_mmc=2; LCDStr(0,WP,0); Delay(500000);	}
		}
	else 
		{state_mmc=3;	LCDStr(0,CP,0);	Delay(100000);}
	return state_mmc;
		}
//////////////////////////////////////////////////////////////////////////////////////////////

		void printdigit(int lx, int ly,int digit)
		{
		switch(digit)
			{
			case 0 : LCDChrXY(lx,ly,'0');
				break;
			case 1 : LCDChrXY(lx,ly,'1');
				break;
			case 2 : LCDChrXY(lx,ly,'2');
				break;
			case 3 : LCDChrXY(lx,ly,'3');
				break;
			case 4 : LCDChrXY(lx,ly,'4');
				break;
			case 5 : LCDChrXY(lx,ly,'5');
				break;
			case 6 : LCDChrXY(lx,ly,'6');
				break;
			case 7 : LCDChrXY(lx,ly,'7');
				break;
			case 8 : LCDChrXY(lx,ly,'8');
				break;
			case 9 : LCDChrXY(lx,ly,'9');
				break;
				}
		}
//5//////////////////////////////////////////////////////////////////////////////////////////		
void test_MMA(void)
{
		////////////MMA pins configuration - Sleep mode disabled///////////////////////////////
			TRISDbits.TRISD0=0;
			PORTDbits.RD0=1;
			//GS1 =0
			TRISDbits.TRISD1=0;
			PORTDbits.RD1=0;
			//GS2=0
			TRISDbits.TRISD2=0;
			PORTDbits.RD2=0;
			
			TRISAbits.TRISA0=1;
			TRISAbits.TRISA1=1;
			TRISAbits.TRISA2=1;
			//ADC module enable
			//ADCON1=0x10111110;			//right-justified; AD=20AD;A/D clock=fOSC/64 	
		    ADCON0=0x01;			//Enable ADC module
			ADCON1=0x10;		
		////////////////////////MMA measure/////////////////////
			resx=0;
			x=0;
			y=0;
			z=0;
			sym=0;
			PORTDbits.RD0=1;  //MMA SLEEP MODE disable
//****************************************************************************
			ANCON0bits.PCFG0=0;  //AN0 - analog input
			ADCON1=0b10111110;			//right-justified; AD=20AD;A/D clock=fOSC/64 	
		//	ADCON1=0x10;
			ADCON0bits.ADON=1; 	//Enable ADC module, AVdref, AVss
			//set Z-axis measure
			ADCON0bits.CHS0=0;
			ADCON0bits.GO=1;				//BSF ADCON0,GO ;Start conversion
			while(ADCON0bits.GO);			//	BSC ADCON0,GO ;Is conversion done? ;	GOTO $-1 ;No, test again
			resz=ADRESL;
			z=ADRESH;
			z=(z<<8)|resz;	
			resz=z;		
			ADCON0=0x00;		//stop ADC
//****************************************************************************
			ANCON0bits.PCFG1=0; 	//AN1 -analog input
			ADCON1=0b10111110;			//right-justified; AD=20AD;A/D clock=fOSC/64 	
			//ADCON1=0x10;
			ADCON0bits.ADON=1; 	//Enable ADC module, AVdref, AVss
			//set Y-axis measure
			ADCON0bits.CHS0=1;
			ADCON0bits.GO=1;				//BSF ADCON0,GO ;Start conversion
			while(ADCON0bits.GO);			//	BSC ADCON0,GO ;Is conversion done? ;	GOTO $-1 ;No, test again
			resy=ADRESL;
			y=ADRESH;
			y=(y<<8)|resy;
			resy=y;
			ADCON0=0x00;		//stop ADC
//*****************************************************************
			ANCON0bits.PCFG2=0; 	//AN2 -analog input
			ADCON1=0b10111110;			//right-justified; AD=20AD;A/D clock=fOSC/64 	
		//	ADCON1=0x10;
			ADCON0bits.ADON=1; 	//Enable ADC module, AVdref, AVss
			//set X-axis measure
			ADCON0bits.CHS0=0;						
			ADCON0bits.CHS1=1;
			ADCON0bits.GO=1;				//BSF ADCON0,GO ;Start conversion
			while(ADCON0bits.GO);			//	BSC ADCON0,GO ;Is conversion done? ;	GOTO $-1 ;No, test again
			resx=ADRESL;
			x=ADRESH;
			x=(x<<8)|resx;
			resx=x;
			ADCON0=0x00;
			////////////////////Write to LCD MMA  measurements////////////////////////////////////
/*
///////////////////////////////X-axis///////////////////////////////////////////////
			sprintf(buf,"X=0x%04x\0",x);
			LCDStr(0,buf,0);
////////////////////////Y-axis///////////////////////////////////////////					
			sprintf(buf,"Y=0x%04x\0",y);
			LCDStr(1,buf,0);
//////////////////////////////Z-axis/////////////////////////////////////////
			sprintf(buf,"Z=0x%04x\0",z);
			LCDStr(2,buf,0);
		*/
			sprintf(buf,"X=0x");
			LCDStr(0,buf,0);
if((resx / 1000)>0)
			{
				sym=resx % 1000;
				LCDChrXY(24,0,sym);
			}
			if ((resx / 100)>0)
			{ 
				sym=resx / 100;
				resx=resx-sym*100;
				if (sym>9)
					sym=sym-10;
			  	printdigit(30,0,sym);//sym
			
			}
			
			if ((resx / 10)>0)
			{ 
				sym=resx / 10;
				resx=resx-sym*10;
				if (sym>9)
					sym=sym-10;
			  	printdigit(36,0,sym);//sym
				printdigit(42,0,resx); //resx
			}
			else {
				sym=resx % 10;
				printdigit(36,0,0);
				printdigit(42,0,sym);//sym
			}
	
			
			sprintf(buf,"Y=0x");
			LCDStr(1,buf,0);
					if((resy / 1000)>0)
			{
				sym=resy % 1000;
				LCDChrXY(24,1,sym);
			}
			if ((resy / 100)>0)
			{ 
				sym=resy / 100;
				resy=resy-sym*100;
				if (sym>9)
					sym=sym-10;
			  	printdigit(30,1,sym);//sym
			
			}
			
			if ((resy / 10)>0)
			{ 
				sym=resy / 10;
				resy=resy-sym*10;
				if (sym>9)
					sym=sym-10;
			  	printdigit(36,1,sym);//sym
				printdigit(42,1,resy); //resx
			}
			else {
				sym=resy % 10;
				printdigit(36,1,0);
				printdigit(42,1,sym);//sym
			}
			
			sprintf(buf,"Z=0x");
			LCDStr(2,buf,0);
				if((z /1000)>0)
			{
				sym=resz % 1000;
				LCDChrXY(24,2,sym);
			}
			if ((resz / 100)>0)
			{ 
				sym=resz / 100;
				resz=resz-sym*100;
				if (sym>9)
					sym=sym-10;
			  	printdigit(30,2,sym);//sym
			
			}
			if ((resz / 10)>0)
			{ 
				sym=resz / 10;
				resz=resz-sym*10;
				if (sym>9)
					sym=sym-10;
			  	printdigit(36,2,sym);//sym
				printdigit(42,2,resz); //resx
			 }
			else {
				sym=resz % 10;
				printdigit(36,2,0);
				printdigit(42,2,sym);//sym
			} 
			Delay(50000);	
			LCDUpdate();
}
///////////////////////////////////////////////////////////////////////////////////////////
/*
int check_bat_sense(void)
{
			ANCON0bits.PCFG4=0; 	//AN4 -analog input
			ADCON1=0b10111110;			//right-justified; AD=20AD;A/D clock=fOSC/64 	
			//ADCON1=0x10;
			ADCON0bits.ADON=1; 	//Enable ADC module, AVdref, AVss
			//set BAT_SENSE measure
			ADCON0bits.CHS2=1;
			ADCON0bits.GO=1;				//BSF ADCON0,GO ;Start conversion
			while(ADCON0bits.GO);			//	BSC ADCON0,GO ;Is conversion done? ;	GOTO $-1 ;No, test again
			res_sense=ADRESL;
			bat_sense=ADRESH;
			bat_sense=(bat_sense<<8)|res_sense;
			//res_sense=bat_sense;
			ADCON0=0x00;		//stop ADC
			if(bat_sense>10) return 1;
				else return 0;
}
*/
//6//////////////////////////////////////////////////////////////////////////////////////////
void test_joystick(void)
{
unsigned char counter = 0;
unsigned char i = 1;
		//Test jopystick and LEDs
	while(i)
	{
		

		if (joystick_left()&& joystick_select() && joystick_down()){
			LCDChrXY (24,2,'L');//	InitLED1();
			counter = 0;
		}
		//	else PORTEbits.RE2=1;
		if (joystick_right()&& joystick_select()&& joystick_up() ){
			LCDChrXY (48,2,'R');//InitLED2();
			counter = 0;
		}
		//	else PORTEbits.RE3=1;
    	if (joystick_right()&& joystick_select()&& joystick_down() ){
			LCDChrXY (36,3,'D');
			counter++;
			if(counter == 2){
				i = 0;
				counter = 0;
			}
		}	
		if (joystick_left()&& joystick_select() && joystick_up()){
			LCDChrXY (36,1,'U');
			counter = 0;
		}
			
		if (joystick_left()&& joystick_select() )
			LCDChrXY (24,1,'1');
		if (joystick_up()&& joystick_select() )
			LCDChrXY (48,1,'2');
		if (joystick_down()&& joystick_select() )
			LCDChrXY (24,3,'3');
		if (joystick_right()&& joystick_select() )
			LCDChrXY (48,3,'4');


		if (joystick_select() )
			LCDChrXY (36,2,'C');

		Delay(100000);			
		LCDUpdate();
	}
}
////////////////////////////////////////////////////////////////////////////////////////
static void InitializeSystem(void)
{
	//On the PIC18F87J50 Family of USB microcontrollers, the PLL will not power up and be enabled
	//by default, even if a PLL enabled oscillator configuration is selected (such as HS+PLL).
	//This allows the device to power up at a lower initial operating frequency, which can be
	//advantageous when powered from a source which is not gauranteed to be adequate for 48MHz
	//operation.  On these devices, user firmware needs to manually set the OSCTUNE<PLLEN> bit to
	//power up the PLL.

	#if defined(__18F87J50)||defined(__18F86J55)|| \
    	defined(__18F86J50)||defined(__18F85J50)|| \
    	defined(__18F67J50)||defined(__18F66J55)|| \
    	defined(__18F66J50)||defined(__18F65J50)

    unsigned int pll_startup_counter = 600;

//I comment next line to enable debugging somehow///////////////////////////////////////////////////
    OSCTUNEbits.PLLEN = 1;  //Enable the PLL and wait 2+ms until the PLL locks before enabling USB module

    while(pll_startup_counter--);
    //Device switches over automatically to PLL output after PLL is locked and ready.

	//Configure all I/O pins to use digital input buffers.  The PIC18F87J50 Family devices
	//use the ANCONx registers to control this, which is different from other devices which
	//use the ADCON1 register for this purpose.
    WDTCONbits.ADSHR = 1;			// Select alternate SFR location to access ANCONx registers
    ANCON0 = 0xFF;                  // Default all pins to digital
    ANCON1 = 0xFF;                  // Default all pins to digital
    WDTCONbits.ADSHR = 0;			// Select normal SFR locations
//	ADCON1 |= 0x0F;   //I put that here

    #elif defined(PIC18F4550_PICDEM_FS_USB)
    ADCON1 |= 0x0F;                 // Default all pins to digital

    #else
        #error Double Click this message.  Please make sure the InitializeSystem() function correctly configures your hardware platform.
		//Also make sure the correct board is selected in usbcfg.h.  If 
		//everything is correct, comment out the above "#error ..." line
		//to suppress the error message.
    #endif


    #if defined(USE_USB_BUS_SENSE_IO)
    tris_usb_bus_sense = INPUT_PIN; // See io_cfg.h
    #endif

    #if defined(USE_SELF_POWER_SENSE_IO)
    tris_self_power = INPUT_PIN;
    #endif
    
    mInitializeUSBDriver();         // See usbdrv.h
    UserInit();                     // See user.c & .h

}//end InitializeSystem
/////////////////////////////////////////////////////////////////////////////////////////
void USBTasks(void)
{
    /*
     * Servicing Hardware
     */
    USBCheckBusStatus();                    // Must use polling method
    if(UCFGbits.UTEYE!=1)
        USBDriverService();                 // Interrupt or polling method

}// end USBTasks




////////////////////////////////////////////////////////////////////////////////////////////
void UpdateMenu(unsigned char pos) {

  Inversion[pos-1] = 1;

  LCDUpdate();
  LCDClear();
  LCDStr ( 0, Menu[0+offset], Inversion[0]);
  LCDStr ( 1, Menu[1+offset], Inversion[1]);
  LCDStr ( 2, Menu[2+offset], Inversion[2]);
  LCDStr ( 3, Menu[3+offset], Inversion[3]);
  LCDStr ( 4, Menu[4+offset], Inversion[4]);
  LCDStr ( 5, Menu[5+offset], Inversion[5] );
  Delay(65000);	
 // LCDUpdate();

  Inversion[pos-1] = 0;
}
//////////////////////////////////////////////////////////////////////////////////////////////
unsigned char GetJoystickPosition (void) {

 
  if (joystick_up() && joystick_left()&& joystick_select()) 
      { return KEY_UP; }
  if (joystick_down() && joystick_right()&& joystick_select())
     { return KEY_DOWN; }
  if (joystick_left()&& joystick_select() && joystick_down()) 
	  { return KEY_LEFT; }
  if (joystick_right()&& joystick_select()&& joystick_up())  
	{ return KEY_RIGHT; }

  if (joystick_select()) return KEY_CENTER;

  return KEY_NONE;

}
///////////////////////////////MAIN PROGRAM////////////////////////////////////////
void main(void)
{
				//	int i,j;
				//		initSSP();
				//	while(1)
				//		spiSendByte(0xf0);
			TRISEbits.TRISE3 = 0;
			TRISEbits.TRISE2 = 0;
			IPR1bits.TMR1IP=1; //TMR1 High priority
			RCONbits.IPEN = 1; //enable interrupt level priority

			//Timer1 RTC configuration
			TMR1H=0x80;		//preload for 1 sec 
			TMR1L=0x0;
			T1CONbits.T1CKPS1=0;
			T1CONbits.T1CKPS0=0;//prescaler 1:1 value
			
			T1CONbits.T1OSCEN=1;//enable Timer 1 Oscillator
			T1CONbits.T1SYNC=1; //do not synchronize external clock
			T1CONbits.TMR1CS=1;	//select external clock source
			T1CONbits.TMR1ON=1; //enable Timer 1
		
			PIE1bits.TMR1IE=1;		//enable Timer 1 interrupt
			INTCONbits.GIEL = 1;	//enable peripheral interrupts 
			INTCONbits.GIEH = 1;	//enable global interrupts
///////////////////LCD configuration/////////////////////////////////////////////////
				LCDInit();
				LCDContrast(0x45);
				LCDClear();
				LCDUpdate();
	
			  for(i=0; i<((48*84)/8); i++)
			   {
			    LCDSend( 0x00, SEND_CHR );
			   }  
			 
	          LCDProba (0, 0, 0x08);
			  LCDProba (1, 0, 0x7E);
			  LCDProba (2, 0, 0x09);
			  LCDProba (3, 0, 0x01);
			  LCDProba (4, 0, 0x02);

			while(1)	
				{
		
	start:		UpdateMenu(MenuPos);
				
				  while(1)
					 {
					test_state=0;
				    JoyPos = GetJoystickPosition();
				
				    if(JoyPos==KEY_UP)
					 {
				      	MenuPos--;
				    	  if((MenuPos<1)) MenuPos = 1;
				      	UpdateMenu(MenuPos);
				     // 	for(dly=0; dly<700000; dly++);
				    }	
				
				    if(JoyPos==KEY_DOWN) {
				      MenuPos++;
				      if((MenuPos>6)) MenuPos = 6;
				      UpdateMenu(MenuPos);
			//	      for(dly=0; dly<700000; dly++);
				    }
				
				   if(JoyPos==KEY_RIGHT)
					 break;
				
				  }
////////////////////////TEST Extesion Port////////////////////////////////////////
				 if(MenuPos == 1)
				 {
				    LCDClear();
				    LCDUpdate();
				AllAsInput();
				PORTG=0b00011111;
				TRISG=0b00011111;
	//			while (!(joystick_left()&& joystick_select() && joystick_down()) );
			    if(TestExt1())	test_state=1;
					else 	test_state=0;
				if(test_state) 
					{
				      LCDInit();
				      LCDContrast(0x45);
					  LCDClear();

				      LCDStr ( 0, ext_test_state, 0 );
					  LCDStr ( 1, ext_test_state2, 0 );
					if(vcc_state) 
					  LCDStr ( 2, VCC_test_state, 0 );
					if(gnd_state) 
					  LCDStr ( 3, GND_test_state, 0 );
					  Delay(500000);
				//      LCDUpdate();
					if(vcc_state || gnd_state){
						Delay(250000);	
						//Delay(500000);	
						//while(1);
						}	
				    }
				    else {
				
				      LCDInit();
				      LCDContrast(0x45);
								
				      LCDClear();
				      LCDStr ( 0, ext_test_state, 0 );
					  LCDStr ( 1, ext_test_state2, 0 );
					if(vcc_state) 
					  LCDStr ( 2, VCC_test_state, 0 );
					if(gnd_state) 
					  LCDStr ( 3, GND_test_state, 0 );
				      Delay(500000);
				      LCDUpdate();
					while(1)	LCDUpdate();
				    }
				test_state=0;
				}
//while (!(joystick_left()&& joystick_select() && joystick_down()) );
	if(MenuPos == 2) {		
		if(TestUEXT())	test_state=1;
			else 	test_state=0;
					if(test_state) 					
					{
				      LCDInit();
				      LCDContrast(0x45);
					  LCDClear();

				      LCDStr ( 2, uext_test_state, 0 );
				      Delay(500000);
				      //LCDUpdate();
					//  while(1);
				    }
				    else {
				
				      LCDInit();
				      LCDContrast(0x45);
								
				      LCDClear();
				      LCDStr ( 2, uext_test_state, 0 );
				      Delay(500000);
				      LCDUpdate();
					 // while(1);
				    }
					goto start;	
				}
////////////////////////TEST MMC/////////////////////////////////////////////////
				 if(MenuPos == 3) 
				{	 
					LCDUpdate();
				    test_state = test_mmc();
				    if(test_state == 0) 
					{
				      LCDClear();
					  LCDStr(0,TestMMC_s,0);
				      Delay(100000);	
				      LCDUpdate();
				    }
						else if (test_state == 1)
						{
				      LCDClear();
					  LCDStr(0,TestMMC_e,0);
					  Delay(100000);	
					  LCDUpdate();
				   		 } 
				goto start;
				}			
/////////////////////////TEST USB///////////////////////////////////////////////
			if(MenuPos==4)
				{	LCDClear();
				    LCDUpdate();//check this
					 
					if(PORTEbits.RE5==1)
						{	
						LCDStr(0,USB_C,0);
	  					Delay(300000);	
						LCDUpdate();	//clear screen 
						InitializeSystem();
						while(1)	   
					    	{
							 USBTasks();         // USB Tasks
						     ProcessIO(); 
							
						//	if (!check_bat_sense())	LCDStr(1,BAT_Off,0);   //LCDStr(1,BAT_On,0);else	LCDStr(1,BAT_Off,0);
						/*
							LCDStr(2,put_BAT,0);
							while(!check_bat_sense())	;
							LCDStr(3,BAT_On,0);
							LCDUpdate();
						*/

							LCDStr(5,Exit,0);

							if(GetJoystickPosition()==KEY_DOWN)	break;
							}
						LCDUpdate();	//clear screen 
						}
						else{
							LCDStr(0,USB_NC,0);
							Delay(500000);
							//Delay(500000);
						}  					
 				goto start;
				}
////////////////////////TEST MMA/////////////////////////////////////////////////
				 if(MenuPos == 5) 
				{
					//	TRISDbits.TRISD0=0;//enable accelerometer SLEEP mode 
					//	PORTDbits.RD0=0;
						LCDUpdate();	
				//		UpdateMenu(MenuPos);
				while(1)	   
					 {
					 if(PORTDbits.RD0==1)   test_MMA();
							else {test_MMA(); LCDStr(3,MMA_D,0);}
						LCDUpdate();
					if(GetJoystickPosition()==KEY_DOWN)	break;
							}
					//UpdateMenu(MenuPos);
				    goto start;
				}		
////////////////////////TEST JOYSTICK/////////////////////////////////////////////////
				 if(MenuPos == 6) 
				{	 
					LCDUpdate();
				    test_joystick();
				    goto start;
				}				
////////////////////////////////clear display/////////////////////////////
				    LCDUpdate();

			}
	
}