#include	<p18f67j50.h>
#include <stdio.h>
#include <string.h>
#include "test.h"

const unsigned char mask_port_a  =0b00100000;
const unsigned int mask_port_b  = 0b11001100 ; 
const unsigned char mask_port_c  =0b11000000;
const unsigned char mask_port_e  =0b11110000;//what a USB_SENSE
const unsigned char mask_port_f  =0b00100100;
const unsigned char mask_port_g  =0b00011111;
int test=0;
unsigned char vcc_state=0;
unsigned char gnd_state=0;
unsigned char VCC_test_state[15];
unsigned char GND_test_state[15];
unsigned char ext_test_state[20];
unsigned char uext_test_state[20];
unsigned char ext_test_state2[20];

/*
CONFIGURATION
ZA EXT1 NE SE TESTVAT : 
RA3(PULL-UP1),
RA5(BAT-SENSE),
//RB6(POLZVAN OT ICD),
/RB7(POLZVAN OT ICD),
RE2(LED1),
RE3(LED2)
RC2(PULL-UP2),
RC3(UEXT_PULL-UP),
*/

void AllAsInput(void) {

	// Port A initialization
//	PORTA=PORTA &  0b00100000;
//	TRISA=TRISA |  0b00100000;

	//Port B initialization
	PORTB=PORTB &  0x00;//0b11111111;
	TRISB=TRISB | 0b11111111;

	// Port C initialization
	PORTC=PORTC & 0x00; // 0b11000100;
	TRISC =TRISC | 0b11000000;
//	PORTC=PORTC & 0x00;
	// Port E initialization
	PORTE=PORTE &  0x00;//0b11011100;
	TRISE=TRISE | 0b11111100;
	
	// Port F initialization
	WDTCONbits.ADSHR=1;
	ANCON0=0x80;
	ANCON1=0x0C;
	WDTCONbits.ADSHR=0;
	PORTF=PORTF &  0x00;//0b00100100;
	TRISF=TRISF | 0b00100100;
	
	// Port G initialization
	PORTG=PORTG &  0x00;//0b00011111;
	TRISG=TRISG &  0b00011111;
	
}



void PinAsOutputLowPortG(char pin) {

  switch (pin) {
    case  0: PORTG=PORTG & 0b11111110;	TRISG=~(0x01); break;//TRISG=0xFF; TRISGbits.TRISG0=0;
    case  1: PORTG=PORTG & 0b11111101;	TRISG=~(0x02);	break;//TRISG=0xFF; TRISGbits.TRISG1=0;
    case  2: PORTG=PORTG & 0b11111011;	TRISG=~(0x04);	break;//TRISG=0xFF; TRISGbits.TRISG2=0;
    case  3: PORTG=PORTG & 0b11110111;	TRISG=~(0x08);	break;//TRISG=0xFF; TRISGbits.TRISG3=0;
    case  4: PORTG=PORTG & 0b11101111;	TRISG=~(0x10);	break;//TRISG=0xFF; TRISGbits.TRISG4=0;
    case  5: PORTG=PORTG & 0b11011111;	//TRISGbits.TRISG5=0;	break;
    case  6: PORTG=PORTG & 0b10111111;//	TRISGbits.TRISG6=0;	break;
    case  7: PORTG=PORTG & 0b01111111;//	TRISGbits.TRISG7=0; break;
      }

}


void PinAsOutputLowPortB(char pin) {

  switch (pin) {
    case  0: PORTB=PORTB & 0b11111110;	TRISB=~(0x01);break; //TRISB=TRISB | 0x02; //DDRB | 
    case  1: PORTB=PORTB & 0b11111101;	TRISB=~(0x02);break;//TRISB=TRISB | 0x02; 
    case  2: PORTB=PORTB & 0b11111011;	TRISB=~(0x04);break; //TRISB=TRISB | 0x02; 
    case  3: PORTB=PORTB & 0b11110111;	TRISB=~(0x08);break;//TRISB=TRISB | 0x02;
    case  4: PORTB=PORTB & 0b11101111;	TRISB=~(0x10);break; //TRISB=TRISB | 0x02; 
    case  5: PORTB=PORTB & 0b11011111;	TRISB=~(0x20);break;//TRISB=TRISB | 0x02; 
    case  6: PORTB=PORTB & 0b10111111;	TRISB=~(0x40);break;//TRISB=TRISB | 0x02;
    case  7: PORTB=PORTB & 0b01111111;	TRISB=~(0x80);break;//TRISB=TRISB | 0x02; 
    
  }

}

void PinAsOutputLowPortC(char pin) {

  switch (pin) {
    case  0: PORTC=PORTC & 0b11111110;	TRISC=~(0x01 | 0x02); 	break;
    case  1: PORTC=PORTC & 0b11111101;	TRISC=~(0x02 | 0x02);	break;
    case  2: PORTC=PORTC & 0b11111011;	TRISC=~(0x04 | 0x02);	break;
    case  3: PORTC=PORTC & 0b11110111;	TRISC=~(0x08 | 0x02);	break;
    case  4: PORTC=PORTC & 0b11101111;	TRISC=~(0x10 | 0x02);	break;
    case  5: PORTC=PORTC & 0b11011111;	TRISC=~(0x20 | 0x02);	break;
    case  6: PORTC=PORTC & 0b10111111;	TRISC=~(0x40 | 0x02);	break;
    case  7: PORTC=PORTC & 0b01111111;	TRISC=~(0x80 | 0x02);  break;
      }

}

void PinAsOutputLowPortE(char pin) {

  switch (pin) {
    case  0: PORTE=PORTE & 0b11111110;	TRISE=~(0x01); 	break;
    case  1: PORTE=PORTE & 0b11111101;	TRISE=~(0x02);	break;
    case  2: PORTE=PORTE & 0b11111011;	TRISE=~(0x04);	break;
    case  3: PORTE=PORTE & 0b11110111;	TRISE=~(0x08);	break;
    case  4: PORTE=PORTE & 0b11101111;	TRISE=~(0x10);	break;
    case  5: PORTE=PORTE & 0b11011111;	TRISE=~(0x20);	break;
    case  6: PORTE=PORTE & 0b10111111;	TRISE=~(0x40);	break;
    case  7: PORTE=PORTE & 0b01111111;	TRISE=~(0x80);  break;
      }

}

void PinAsOutputLowPortD(char pin) {

  switch (pin) {
    case  0: PORTD=PORTD & 0b11111110;	TRISD=~(0x01); 	break;
    case  1: PORTD=PORTD & 0b11111101;	TRISD=~(0x02);	break;
    case  2: PORTD=PORTD & 0b11111011;	TRISD=~(0x04);	break;
    case  3: PORTD=PORTD & 0b11110111;	TRISD=~(0x08);	break;
    case  4: PORTD=PORTD & 0b11101111;	TRISD=~(0x10);	break;
    case  5: PORTD=PORTD & 0b11011111;	TRISD=~(0x20);	break;
    case  6: PORTD=PORTD & 0b10111111;	TRISD=~(0x40);	break;
    case  7: PORTD=PORTD & 0b01111111;	TRISD=~(0x80);  break;
      }

}

void PinAsOutputLowPortF(char pin) {

  switch (pin) {
    case  0: PORTF=PORTF & 0b11111110;	TRISF=~(0x01); 	break;
    case  1: PORTF=PORTF & 0b11111101;	TRISF=~(0x02);	break;
    case  2: PORTF=PORTF & 0b11111011;	TRISF=~(0x04);	break;
    case  3: PORTF=PORTF & 0b11110111;	TRISF=~(0x08);	break;
    case  4: PORTF=PORTF & 0b11101111;	TRISF=~(0x10);	break;
    case  5: PORTF=PORTF & 0b11011111;	TRISF=~(0x20);	break;
    case  6: PORTF=PORTF & 0b10111111;	TRISF=~(0x40);	break;
    case  7: PORTF=PORTF & 0b01111111;	TRISF=~(0x80);  break;
      }

}

void PullUp1High(void) {

  // RA3 => output, high
	  PORTAbits.RA3=1;	
	  TRISAbits.TRISA3=0;
	  PORTAbits.RA3=1;
}

void PullUp1Low(void) {

  // RA3 => output, low
  	  PORTAbits.RA3=0;
	  TRISAbits.TRISA3=0;
	  PORTAbits.RA3=0;
}

void PullUp2High(void) {

  // RA3 => output, high
	  PORTCbits.RC2=1;	
	  TRISCbits.TRISC2=0;
	  PORTCbits.RC2=1;
}

void PullUp2Low(void) {

  // RA3 => output, low
  	  PORTCbits.RC2=0;
	  TRISCbits.TRISC2=0;
	  PORTCbits.RC2=0;
}

int TestExt1(void)
{
	int i;
sprintf(ext_test_state,"TEST EXT 1 OK");
sprintf(ext_test_state2,"TEST EXT 2 OK");
//TestExt1=================================================================
// Test for GND ==============================================================
 	AllAsInput();
  	PullUp1High();
	AllAsInput();
	PullUp2High();

    Delayc(100);
	
   if((unsigned char)((PORTB)|(~mask_port_b)) != 0xFF) 
	  		{
		sprintf(GND_test_state,"GND PORTB");
		gnd_state=1;
		//	return 0;
			 }
   Delayc(100);
	if((unsigned char)((PORTG)|(~mask_port_g)) != 0xFF)
	  		{
		sprintf(GND_test_state,"GND PORTG");
		gnd_state=1;
		//	return 0;
			 }
	if((unsigned char)((PORTC)|(~mask_port_c)) != 0xFF)
	  	  		{
		sprintf(GND_test_state,"GND PORTC");
		gnd_state=1;
		//	return 0;
			 }
	  
	if((unsigned char)((PORTE)|(~mask_port_e)) != 0xFF)
	  	  		{
		sprintf(GND_test_state,"GND PORTE");
		gnd_state=1;
		//	return 0;
			 }
	
	if((unsigned char)((PORTF)|(~mask_port_f)) != 0xFF)
	  		{
		sprintf(GND_test_state,"GND PORTF");
		gnd_state=1;
		//	return 0;
			 }
   // End =======================================================================


  // Test for VCC ==============================================================
  AllAsInput();
  PullUp1Low();
  AllAsInput();
  PullUp2Low();
//PORTC=0x00;
  Delayc(100);

	  if((PORTB & mask_port_b) != 0x0) 
	  		{
		sprintf(VCC_test_state,"VCC PORTB");
		vcc_state=1;
		//	return 0;
			 }
	
	 if((PORTG & mask_port_g)!= 0x0) 
	  		{
		sprintf(VCC_test_state,"VCC PORTG");
		vcc_state=1;
		//	return 0;
			 }
	if((PORTC & mask_port_c)!= 0x0) 
	  		{
		sprintf(VCC_test_state,"VCC PORTC");
		vcc_state=1;
		//	return 0;
			 }
	 
	if((PORTE & mask_port_e)!= 0x0) 
	  		{
		sprintf(VCC_test_state,"VCC PORTE");
		vcc_state=1;
			//return 0;
			 }
	 
	if((PORTF & mask_port_f)!= 0x0) 
	  		{
		sprintf(VCC_test_state,"VCC PORTF");
		vcc_state=1;
		//	return 0;
			 }
      
  // End =======================================================================


  // Running zero ==============================================================
  AllAsInput();
  PullUp1High();
  Delayc(100);  
  // PortB
	for(i=0; i<8; i++) {
	
	    // this port is not tested
	    if(!((mask_port_b)&(1<<i))) continue;
	
	    PinAsOutputLowPortB(i);
	
	 
	    if((unsigned char) (PORTB |(~mask_port_b))!= ((0xFF)&(~(1<<i)) ) )
			{
		sprintf(ext_test_state,"PORTB PORTB");
		//	return 0;
			 }
		
		if((unsigned char)(PORTG |(~mask_port_g))!= 0xFF)
			{
		sprintf(ext_test_state,"PORTB PORTG");
		//	return 0;
			 }
	  }
 

///////////////////////////////////////////////////////////////////
  AllAsInput();
  PullUp1High();
  Delayc(100);  
// PortG
  for(i=0; i<8; i++) {

    // this port is not tested
    if(!((mask_port_g)&(1<<i))) continue;

    PinAsOutputLowPortG(i);
	 //Delay(10);

    if((unsigned char)((PORTB) |(~mask_port_b)) != 0xFF)               
		{
	sprintf(ext_test_state,"PORTG PORTB");
		//return 0;
		 }

	if((unsigned char)((PORTG) |(~mask_port_g)) != ((0xFF) &(~(1<<i))))               
			{
	sprintf(ext_test_state,"PORTG PORTG");
		//	return 0;
			 }
}
	//sprintf(ext_test_state,"TEST EXT 1 OK");
// End TEST EXTENSION 1=======================================================================

//***********************************************************************
//TEST EXTENSION 2
AllAsInput();
PullUp2High();
Delayc(100);
// PortC
  for(i=0; i<8; i++) {

	    // this port is not tested
	    if(!((mask_port_c)&(1<<i))) continue;
	
	    PinAsOutputLowPortC(i);
		PullUp2High();
	
	   // Delay(10);
	
	    if((unsigned char)((PORTC) |(~mask_port_c)) != ((0xFF)&(~(1<<i))) )               
				{
		sprintf(ext_test_state2,"PORTC PORTC");
		//		return 0;
				 }
		if((unsigned char)((PORTE) |(~mask_port_e)) != 0xFF)               
				{
		sprintf(ext_test_state2,"PORTC PORTE");
			//	return 0;
				 }
		if((unsigned char)((PORTF) |(~mask_port_f)) != 0xFF)               
				{
		sprintf(ext_test_state2,"PORTC PORTF");
			//	return 0;
				 }
		}
///////////////////////////////////////////////////////////////////////
  AllAsInput();
  PullUp2High();
  Delayc(100);

  // PortE
  for(i=0; i<8; i++) {

    // this port is not tested
    if(!((mask_port_e)&(1<<i))) continue;

    PinAsOutputLowPortE(i);

    //Delay(10);

	if((unsigned char)((PORTC) |(~mask_port_c)) != 0xFF)               
			{
	sprintf(ext_test_state2,"PORTE PORTC");
		//	return 0;
			 }
	if((unsigned char)((PORTE) |(~mask_port_e)) != ((0xFF)&(~(1<<i))) )               
			{
	sprintf(ext_test_state2,"PORTE PORTE");
		//	return 0;
			 }
	if((unsigned char)((PORTF) |(~mask_port_f)) != 0xFF)               
			{
	sprintf(ext_test_state2,"PORTE PORTF");
		//	return 0;
			 }
	}
///////////////////////////////////////////////////////////////////
  AllAsInput();
  PullUp2High();
  Delayc(100);

  // PortF
  for(i=0; i<8; i++) {

    // this port is not tested
    if(!((mask_port_f)&(1<<i))) continue;

    PinAsOutputLowPortF(i);

   // Delay(10);

    if((unsigned char)((PORTC) |(~mask_port_c)) != 0xFF)               
			{
	sprintf(ext_test_state2,"PORTF PORTC");
		//	return 0;
			 }
	if((unsigned char)((PORTE) |(~mask_port_e)) != 0xFF)               
			{
	sprintf(ext_test_state2,"PORTF PORTE");
		//	return 0;
			 }
	if((unsigned char)((PORTF) |(~mask_port_f)) != ((0xFF)&(~(1<<i))))               
			{
	sprintf(ext_test_state2,"PORTF PORTF");
	//		return 0;
			 }
	 }
//sprintf(ext_test_state2,"TEST EXT 2 OK");
// End TEST EXTENSION 1=======================================================================

//*****************************************************
//Also do cross-check of extensions
//1)Pullup1-low,check ext2 port;
AllAsInput();
PullUp1Low();
PullUp2High();
//Delay(10);
/*
if(PORTBbits.RB2==1)
	{
	sprintf(ext_test_state2,"0  EXT1 EXT2");
		//	return 0;
	}*/
//2)Pull-up2 high,check ext1 port
AllAsInput();
PullUp2Low();
PullUp1High();
//Delay(10);
/*if(PORTFbits.RF2==1)
	{
	sprintf(ext_test_state2,"0 EXT2 EXT1");
		//	return 0;
	}
*/
	AllAsInput();
	return 1;
}
//End*********************************************************



//UEXT=====================================
/**********************************************
RC3(SSP1)PULLUP
RC4(SSP1)
RC5(SSP1)
RC6(SSP1)
RC7(SSP1)
RD6(SSP2)
RD7(SSP2)
RE4(CS_UEXT)
************************************************/
const unsigned char mask_port_c_uext  =0b11110000;
const unsigned char mask_port_d_uext  =0b01100000;
const unsigned char mask_port_e_uext  =0b00010000;
//const unsigned char mask_port_c  =0b11000000;

void AllUEXTAsInput(void)
 {

 SSP1CON1=0;//bits.SSPEN=0;
 SSP2CON1=0x00;//bits.SSPEN=0;
//PORTC
PORTC=0x00;
TRISC |= 0b11110000;

//PORTD
PORTD=0x00;
TRISD |= 0b01100000;

//PORTE
PORTEbits.RE4=0;
TRISEbits.TRISE4=1;

}
void PullUpUEXTHigh(void) {

  // RC3 => output, high
	  
	  SSP1CON1=0;//bits.SSPEN=0;
	  SSP2CON1=0;//bits.SSPEN=0;
	  PORTCbits.RC3=1;	
	  TRISCbits.TRISC3=0;
	  PORTCbits.RC3=1;
}

void PullUpUEXTLow(void) {

  // RA3 => output, low
	SSP1CON1=0;//bits.SSPEN=0;
	SSP2CON1=0;//bits.SSPEN=0;
  	 PORTCbits.RC3=0; 	//PORTC=PORTC & 0b11110111;	TRISC=~(0x08);
	  TRISCbits.TRISC3=0;
	  PORTCbits.RC3=0;//TRISC=~(0x08);	//PORTC=PORTC & 0b11110111;	
}
int TestUEXT(void)
{
		int i;
//Test for GND
AllUEXTAsInput();
PullUpUEXTHigh();
Delayc(100);
if((unsigned char)((PORTC)|(~mask_port_c_uext)) != 0xFF)
	  		{
		sprintf(uext_test_state,"GND UEXT RC");
			return 0;
			 }

if((unsigned char)((PORTD) | (~mask_port_d_uext))!= 0xFF)
		{
	sprintf(uext_test_state,"GND UEXT RD");
			return 0;
		}

if(PORTEbits.RE4==0) 
	{
	sprintf(uext_test_state,"GND UEXT RE");
			return 0;
		}

//Test for VCC
AllUEXTAsInput();
PullUpUEXTLow();
Delayc(100);
Delayc(100);
if((PORTC & mask_port_c_uext)!= 0x0)
		{
	sprintf(uext_test_state,"VCC UEXT RC");
			return 0;
		}
PullUpUEXTLow();
Delayc(100);
if((PORTD & mask_port_d_uext)!= 0x0)
		{
	sprintf(uext_test_state,"VCC UEXT RD");
			return 0;
		}
if(PORTEbits.RE4==1) 
	{
	sprintf(uext_test_state,"VCC UEXT RE");
			return 0;
		}
//Test for run 0

	AllUEXTAsInput();
	PullUpUEXTHigh();
    Delayc(100);
	 // PortC
	  for(i=0; i<8; i++) {

	    // this port is not tested
	    if(!((mask_port_c_uext)&(1<<i))) continue;
	
	    PinAsOutputLowPortC(i);
		PullUpUEXTHigh();
	  //  Delay(10);
		
	    if((unsigned char)((PORTC) |(~mask_port_c_uext)) != ((0xFF)&(~(1<<i))) )               
				{
		sprintf(uext_test_state,"PORTC PORTC");
				return 0;
				 }
		if((unsigned char)((PORTD) |(~mask_port_d_uext)) != 0xFF)               
				{
		sprintf(uext_test_state,"PORTC PORTD");
				return 0;
				 }
		if((unsigned char)((PORTE) |(~mask_port_e_uext)) != 0xFF)               
				{
		sprintf(uext_test_state,"PORTC PORTE");
				return 0;
				 }
		}

// PortD
	AllUEXTAsInput();
	PullUpUEXTHigh();
 	Delayc(100);
	  for(i=0; i<8; i++) {

	    // this port is not tested
	    if(!((mask_port_d_uext)&(1<<i))) continue;
	
	    PinAsOutputLowPortD(i);
	
	  //  Delay(10);
	
	    if((unsigned char)((PORTC) |(~mask_port_c_uext)) != 0xFF )               
				{
		sprintf(uext_test_state,"PORTD PORTC");
				return 0;
				 }
		if((unsigned char)((PORTD) |(~mask_port_d_uext)) != ((0xFF)&(~(1<<i))))               
				{
		sprintf(uext_test_state,"PORTD PORTD");
				return 0;
				 }
		if((unsigned char)((PORTE) |(~mask_port_e_uext)) != 0xFF)               
				{
		sprintf(uext_test_state,"PORTD PORTE");
				return 0;
				 }
		}
// PortE
	AllUEXTAsInput();
	PullUpUEXTHigh();
	Delayc(100);
	
	PORTEbits.RE4=0;
//	Delay(10);

	    if((unsigned char)((PORTC) |(~mask_port_c_uext)) != 0xFF)
				{
		sprintf(uext_test_state,"PORTE PORTC");
				return 0;
				 }
		if((unsigned char)((PORTD) |(~mask_port_d_uext)) != 0xFF)               
				{
		sprintf(uext_test_state,"PORTE PORTD");
				return 0;
				 }
			
		sprintf(uext_test_state,"TEST UEXT OK");
		return 1;
}

